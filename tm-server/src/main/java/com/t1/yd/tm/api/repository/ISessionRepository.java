package com.t1.yd.tm.api.repository;

import com.t1.yd.tm.dto.model.SessionDTO;
import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ISessionRepository extends IUserOwnedRepository<SessionDTO> {

    @Update("UPDATE sessions " +
            "SET user_id = #{userId}, " +
            "   date = #{date}, " +
            "   role = #{role}, " +
            " WHERE id = #{id}")
    void update(@NotNull SessionDTO sessionDTO);


    @Insert("INSERT INTO sessions " +
            "(id, user_id, date, role) " +
            "VALUES (#{id}, #{userId}, #{date}, #{role})")
    void add(@NotNull SessionDTO sessionDTO);


    @Select("SELECT * FROM sessions order by #{sort}")
    @Results({
            @Result(property = "userId", column = "user_id")
    })
    @NotNull List<SessionDTO> findAll(@NotNull String sort);


    @Select("SELECT * FROM sessions WHERE user_id = #{userId} order by #{sort}")
    @Results({
            @Result(property = "userId", column = "user_id")
    })
    @NotNull List<SessionDTO> findAllWithUserId(@Param("userId") @NotNull String userId, @Param("sortField") @NotNull String sort);


    @Delete("DELETE FROM sessions")
    void clear();


    @Delete("DELETE FROM sessions WHERE user_id = #{userId}")
    void clearWithUserId(String userId);


    @Select("SELECT * FROM sessions WHERE id = #{id} LIMIT 1")
    @Results({
            @Result(property = "userId", column = "user_id")
    })
    @Nullable SessionDTO findOneById(@NotNull String id);


    @Select("SELECT * from (SELECT row_number() " +
            "OVER (order by id) as rnum, * FROM sessions) t1 WHERE rnum = #{index}")
    @Results({
            @Result(property = "userId", column = "user_id")
    })
    @Nullable SessionDTO findOneByIndex(@NotNull Integer index);


    @Delete("DELETE FROM sessions WHERE id = #{id}")
    void removeById(@NotNull String id);


    @Select("SELECT * FROM sessions WHERE user_id = #{userId} AND id = #{id}")
    @Results({
            @Result(property = "userId", column = "user_id")
    })
    @Nullable SessionDTO findOneByIdWithUserId(@Param("userId") @NotNull String userId, @Param("id") @NotNull String id);


    @Select("SELECT * FROM (SELECT row_number() " +
            "OVER (order by id) as rnum, * FROM sessions WHERE user_id = #{userId}) t1 WHERE rnum = #{index}")
    @Results({
            @Result(property = "userId", column = "user_id")
    })
    @Nullable SessionDTO findOneByIndexWithUserId(@Param("userId") @NotNull String userId, @Param("index") @NotNull Integer index);

}
