package com.t1.yd.tm.api.service;

import com.t1.yd.tm.dto.model.AbstractEntityDTO;
import com.t1.yd.tm.enumerated.Sort;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IService<E extends AbstractEntityDTO> {

    void clear();

    @NotNull List<E> findAll(@NotNull String sort);

    @NotNull List<E> findAll();

    @NotNull List<E> findAll(@Nullable Comparator comparator);

    @NotNull List<E> findAll(@Nullable Sort sort);

    @NotNull E add(@NotNull E entity);

    @NotNull E update(@NotNull E entity);

    @NotNull Collection<E> add(@NotNull Collection<E> entities);

    @NotNull Collection<E> set(@NotNull Collection<E> entities);

    boolean existsById(@NotNull String id);

    E findOneById(@NotNull String id);

    E findOneByIndex(@NotNull Integer index);

    E removeById(@NotNull String id);

    E removeByIndex(@NotNull Integer index);

    int getSize();

}
