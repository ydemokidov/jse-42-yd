package com.t1.yd.tm.service;

import com.t1.yd.tm.api.repository.IRepository;
import com.t1.yd.tm.api.service.IConnectionService;
import com.t1.yd.tm.api.service.IService;
import com.t1.yd.tm.dto.model.AbstractEntityDTO;
import com.t1.yd.tm.enumerated.Sort;
import com.t1.yd.tm.exception.entity.EntityNotFoundException;
import com.t1.yd.tm.exception.field.IdEmptyException;
import com.t1.yd.tm.exception.field.IndexIncorrectException;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

@AllArgsConstructor
public abstract class AbstractService<E extends AbstractEntityDTO, R extends IRepository<E>> implements IService<E> {

    @NotNull
    protected final IConnectionService connectionService;

    @NotNull
    protected abstract R getRepository(@NotNull final SqlSession sqlSession);

    @NotNull
    @Override
    public List<E> findAll(@Nullable final Sort sort) {
        if (sort == null) return findAll();
        return findAll(sort.getComparator());
    }

    @NotNull
    @Override
    @SneakyThrows
    public E add(@NotNull final E entity) {
        @NotNull final SqlSession sqlSession = connectionService.openSession();
        try {
            @NotNull final R repository = getRepository(sqlSession);
            repository.add(entity);
            sqlSession.commit();
            return entity;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final SqlSession sqlSession = connectionService.openSession();
        try {
            @NotNull final R repository = getRepository(sqlSession);
            repository.clear();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<E> findAll() {
        @NotNull final SqlSession sqlSession = connectionService.openSession();
        try {
            @NotNull final R repository = getRepository(sqlSession);
            return repository.findAll();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<E> findAll(@Nullable final Comparator comparator) {
        @NotNull final SqlSession sqlSession = connectionService.openSession();
        try {
            @NotNull final R repository = getRepository(sqlSession);
            if (comparator == null) return findAll();
            return repository.findAll(comparator);
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E findOneById(@NotNull final String id) {
        @NotNull final SqlSession sqlSession = connectionService.openSession();
        try {
            if (id.isEmpty()) throw new IdEmptyException();
            @NotNull final R repository = getRepository(sqlSession);
            return repository.findOneById(id);
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E findOneByIndex(@NotNull final Integer index) {
        @NotNull final SqlSession sqlSession = connectionService.openSession();
        try {
            if (index < 0) throw new IndexIncorrectException();
            @NotNull final R repository = getRepository(sqlSession);
            return repository.findOneByIndex(index);
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E removeById(@NotNull final String id) {
        @NotNull final SqlSession sqlSession = connectionService.openSession();
        try {
            if (id.isEmpty()) throw new IdEmptyException();
            @Nullable final E result = findOneById(id);
            if (result == null) throw new EntityNotFoundException();
            @NotNull final R repository = getRepository(sqlSession);
            repository.removeById(id);
            sqlSession.commit();
            return result;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public E removeByIndex(@NotNull final Integer index) {
        @NotNull final SqlSession sqlSession = connectionService.openSession();
        try {
            if (index < 0) throw new IndexIncorrectException();
            @NotNull final R repository = getRepository(sqlSession);
            @Nullable final E result = findOneByIndex(index);
            if (result == null) throw new EntityNotFoundException();
            repository.removeByIndex(index);
            sqlSession.commit();
            return result;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean existsById(@NotNull final String id) {
        @NotNull final SqlSession sqlSession = connectionService.openSession();
        try {
            if (id.isEmpty()) throw new IdEmptyException();
            @NotNull final R repository = getRepository(sqlSession);
            return repository.findOneById(id) != null;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public int getSize() {
        @NotNull final SqlSession sqlSession = connectionService.openSession();
        try {
            @NotNull final R repository = getRepository(sqlSession);
            return repository.findAll().size();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public @NotNull Collection<E> set(@NotNull Collection<E> collection) {
        @NotNull final SqlSession sqlSession = connectionService.openSession();
        try {
            @NotNull final R repository = getRepository(sqlSession);
            repository.clear();
            collection.forEach(repository::add);
            sqlSession.commit();
            return collection;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public @NotNull Collection<E> add(@NotNull Collection<E> collection) {
        @NotNull final SqlSession sqlSession = connectionService.openSession();
        try {
            @NotNull final R repository = getRepository(sqlSession);
            collection.forEach(repository::add);
            sqlSession.commit();
            return collection;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public E update(@Nullable E entity) {
        if (entity == null) throw new EntityNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.openSession();
        try {
            @NotNull final IRepository<E> repository = getRepository(sqlSession);
            repository.update(entity);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return entity;
    }


}