package com.t1.yd.tm.api.service;

import com.t1.yd.tm.dto.model.TaskDTO;
import com.t1.yd.tm.enumerated.Status;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface ITaskService extends IUserOwnedService<TaskDTO> {

    @NotNull
    TaskDTO create(@NotNull String userId, @NotNull String name, @NotNull String description);

    @NotNull
    TaskDTO updateById(@NotNull String userId, @NotNull String id, @NotNull String name, @NotNull String description);

    @NotNull
    TaskDTO updateByIndex(@NotNull String userId, @NotNull Integer index, @NotNull String name, @NotNull String description);

    @NotNull
    TaskDTO changeStatusById(@NotNull String userId, @NotNull String id, @NotNull Status status);

    @NotNull
    TaskDTO changeStatusByIndex(@NotNull String userId, @NotNull Integer index, @NotNull Status status);

    @NotNull
    List<TaskDTO> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

    TaskDTO findTaskById(@NotNull String userId, @NotNull String id);

    TaskDTO findTaskByIndex(@NotNull String userId, @NotNull Integer index);

    TaskDTO removeTaskById(@NotNull String userId, @NotNull String id);

    TaskDTO removeTaskByIndex(@NotNull String userId, @NotNull Integer index);

}
