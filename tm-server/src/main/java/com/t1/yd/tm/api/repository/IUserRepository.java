package com.t1.yd.tm.api.repository;

import com.t1.yd.tm.dto.model.UserDTO;
import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IUserRepository extends IRepository<UserDTO> {

    @Select("SELECT * FROM users WHERE login = #{login}")
    @Results({
            @Result(property = "passwordHash", column = "pwd_hash"),
            @Result(property = "firstName", column = "fst_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "mid_name")
    })
    @Nullable UserDTO findByLogin(@NotNull String login);


    @Select("SELECT * FROM users WHERE email = #{email}")
    @Results({
            @Result(property = "passwordHash", column = "pwd_hash"),
            @Result(property = "firstName", column = "fst_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "mid_name")
    })
    @Nullable UserDTO findByEmail(@NotNull String email);


    @Update("UPDATE users " +
            "SET login = #{login}, " +
            "   pwd_hash = #{passwordHash}, " +
            "   email = #{email}, " +
            "   fst_name = #{firstName}," +
            "   last_name = #{lastName}," +
            "   mid_name = #{middleName}," +
            "   role = #{role}," +
            "   locked = #{locked}" +
            " WHERE id = #{id}")
    void update(@NotNull UserDTO userDTO);


    @Insert("INSERT INTO users " +
            "(id, login, pwd_hash, email, fst_name, last_name, mid_name, role, locked) " +
            "VALUES (#{id}, #{login}, #{passwordHash}, #{email}, #{firstName}, #{lastName}, #{middleName}, #{role}, #{locked})")
    void add(@NotNull UserDTO userDTO);


    @Select("SELECT * FROM users order by #{sort}")
    @Results({
            @Result(property = "passwordHash", column = "pwd_hash"),
            @Result(property = "firstName", column = "fst_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "mid_name")
    })
    @NotNull List<UserDTO> findAll(@NotNull String sort);


    @Delete("DELETE FROM users")
    void clear();


    @Select("SELECT * FROM users WHERE id = #{id} LIMIT 1")
    @Results({
            @Result(property = "passwordHash", column = "pwd_hash"),
            @Result(property = "firstName", column = "fst_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "mid_name")
    })
    @Nullable UserDTO findOneById(@NotNull String id);


    @Select("SELECT * FROM (SELECT row_number() " +
            "OVER (order by id) as rnum, * FROM users) t1 WHERE rnum = #{index}")
    @Results({
            @Result(property = "passwordHash", column = "pwd_hash"),
            @Result(property = "firstName", column = "fst_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "mid_name")
    })
    @Nullable UserDTO findOneByIndex(@NotNull Integer index);


    @Delete("DELETE FROM users WHERE id = #{id}")
    void removeById(@NotNull String id);

}
