package com.t1.yd.tm.service;

import com.t1.yd.tm.api.service.IConnectionService;
import com.t1.yd.tm.api.service.IPropertyService;
import com.t1.yd.tm.api.service.ISessionService;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static com.t1.yd.tm.constant.SessionTestData.*;
import static com.t1.yd.tm.constant.UserTestData.ADMIN;
import static com.t1.yd.tm.constant.UserTestData.USER_DTO_1;

public class SessionDTOServiceTest {

    private IPropertyService propertyService = new PropertyService();

    private IConnectionService connectionService = new ConnectionService(propertyService);

    private ISessionService service = new SessionService(connectionService);

    @Before
    public void initRepository() {
        service = new SessionService(connectionService);
    }

    @Test
    public void add() {
        service.add(SESSION_DTO_1);
        Assert.assertEquals(SESSION_DTO_1.getId(), service.findOneById(SESSION_DTO_1.getId()).getId());
    }

    @Test
    public void addWithUser() {
        service.add(USER_DTO_1.getId(), SESSION_DTO_1);
        Assert.assertEquals(SESSION_DTO_1.getId(), service.findAll(USER_DTO_1.getId()).get(0).getId());
    }

    @Test
    public void addAll() {
        service.add(ALL_SESSION_DTOS);
        Assert.assertEquals(ALL_SESSION_DTOS.size(), service.findAll().size());
        Assert.assertEquals(SESSION_DTO_1.getId(), service.findAll().get(0).getId());
    }

    @Test
    public void clear() {
        service.add(ALL_SESSION_DTOS);
        service.clear();
        Assert.assertTrue(service.findAll().isEmpty());
    }

    @Test
    public void findAllByUserId() {
        service.add(USER_DTO_1.getId(), SESSION_DTO_1);
        service.add(ADMIN.getId(), SESSION_DTO_2);
        Assert.assertEquals(1, service.findAll(USER_DTO_1.getId()).size());
    }

    @Test
    public void existsById() {
        service.add(SESSION_DTO_1);
        Assert.assertTrue(service.existsById(SESSION_DTO_1.getId()));
        Assert.assertFalse(service.existsById(SESSION_DTO_2.getId()));
    }

    @Test
    public void removeById() {
        service.add(ALL_SESSION_DTOS);
        service.removeById(SESSION_DTO_1.getId());
        Assert.assertEquals(ALL_SESSION_DTOS.size() - 1, service.findAll().size());
        Assert.assertNull(service.findOneById(SESSION_DTO_1.getId()));
    }

    @Test
    public void findOneByIdWithUserId() {
        service.add(USER_DTO_1.getId(), SESSION_DTO_1);
        service.add(ADMIN.getId(), SESSION_DTO_2);

        Assert.assertEquals(SESSION_DTO_1.getId(), service.findOneById(USER_DTO_1.getId(), SESSION_DTO_1.getId()).getId());
        Assert.assertNull(service.findOneById(ADMIN.getId(), SESSION_DTO_1.getId()));
    }

    @After
    public void clearData() {
        service.clear();
    }

}
