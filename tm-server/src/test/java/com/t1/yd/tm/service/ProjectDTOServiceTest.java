package com.t1.yd.tm.service;

import com.t1.yd.tm.api.service.IConnectionService;
import com.t1.yd.tm.api.service.IProjectService;
import com.t1.yd.tm.api.service.IPropertyService;
import com.t1.yd.tm.enumerated.Status;
import com.t1.yd.tm.marker.UnitCategory;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static com.t1.yd.tm.constant.ProjectTestData.*;
import static com.t1.yd.tm.constant.UserTestData.ADMIN;
import static com.t1.yd.tm.constant.UserTestData.USER_DTO_1;

@Category(UnitCategory.class)
public class ProjectDTOServiceTest {

    private IPropertyService propertyService = new PropertyService();

    private IConnectionService connectionService = new ConnectionService(propertyService);

    private IProjectService service;

    @Before
    public void initRepository() {
        service = new ProjectService(connectionService);
    }

    @Test
    public void add() {
        service.add(USER_1_PROJECT_DTO_1);
        Assert.assertEquals(USER_1_PROJECT_DTO_1.getId(), service.findOneById(USER_1_PROJECT_DTO_1.getId()).getId());
    }

    @Test
    public void addWithUser() {
        service.add(USER_DTO_1.getId(), USER_1_PROJECT_DTO_1);
        Assert.assertEquals(USER_1_PROJECT_DTO_1.getId(), service.findOneById(USER_1_PROJECT_DTO_1.getId()).getId());
    }

    @Test
    public void addAll() {
        service.add(ALL_PROJECT_DTOS);
        Assert.assertEquals(ALL_PROJECT_DTOS.size(), service.getSize());
        Assert.assertEquals(USER_1_PROJECT_DTO_2.getId(), service.findOneById(USER_1_PROJECT_DTO_2.getId()).getId());
    }

    @Test
    public void removeById() {
        service.add(ALL_PROJECT_DTOS);
        service.removeById(USER_1_PROJECT_DTO_2.getId());
        Assert.assertEquals(ALL_PROJECT_DTOS.size() - 1, service.getSize());
        Assert.assertNull(service.findOneById(USER_1_PROJECT_DTO_2.getId()));
    }


    @Test
    public void clear() {
        service.add(ALL_PROJECT_DTOS);
        service.clear();
        Assert.assertTrue(service.findAll().isEmpty());
    }

    @Test
    public void findAllByUserId() {
        service.add(USER_DTO_1.getId(), USER_1_PROJECT_DTO_1);
        service.add(USER_DTO_1.getId(), USER_1_PROJECT_DTO_2);
        service.add(ADMIN.getId(), ADMIN_PROJECT_DTO_1);
        Assert.assertEquals(USER_1_PROJECT_DTOS.size(), service.findAll(USER_DTO_1.getId()).size());
    }

    @Test
    public void findOneByIdWithUserId() {
        service.add(USER_DTO_1.getId(), USER_1_PROJECT_DTO_1);
        service.add(USER_DTO_1.getId(), USER_1_PROJECT_DTO_2);
        service.add(ADMIN.getId(), ADMIN_PROJECT_DTO_1);

        Assert.assertEquals(USER_1_PROJECT_DTO_1.getId(), service.findOneById(USER_DTO_1.getId(), USER_1_PROJECT_DTO_1.getId()).getId());
        Assert.assertNull(service.findOneById(ADMIN.getId(), USER_1_PROJECT_DTO_1.getId()));
    }

    @Test
    public void findOneByIndexWithUserIdPositive() {
        service.add(USER_DTO_1.getId(), USER_1_PROJECT_DTO_1);
        Assert.assertEquals(USER_1_PROJECT_DTO_1.getId(), service.findOneByIndex(USER_DTO_1.getId(), 1).getId());
    }

    @Test
    public void findOneByIndexWithUserIdNegative() {
        service.add(USER_DTO_1.getId(), USER_1_PROJECT_DTO_1);
        Assert.assertNull(service.findOneByIndex(ADMIN.getId(), 1));
    }

    @Test
    public void existsById() {
        service.add(USER_DTO_1.getId(), USER_1_PROJECT_DTO_1);
        Assert.assertTrue(service.existsById(USER_DTO_1.getId(), USER_1_PROJECT_DTO_1.getId()));
        Assert.assertFalse(service.existsById(ADMIN.getId(), USER_1_PROJECT_DTO_2.getId()));
    }

    @Test
    public void updateById() {
        service.add(USER_DTO_1.getId(), USER_1_PROJECT_DTO_1);
        service.updateById(USER_DTO_1.getId(), USER_1_PROJECT_DTO_1.getId(), PROJECT_NAME, PROJECT_DESCRIPTION);
        Assert.assertEquals(PROJECT_NAME, service.findProjectById(USER_DTO_1.getId(), USER_1_PROJECT_DTO_1.getId()).getName());
        Assert.assertEquals(PROJECT_DESCRIPTION, service.findProjectById(USER_DTO_1.getId(), USER_1_PROJECT_DTO_1.getId()).getDescription());
    }

    @Test
    public void updateByIndex() {
        service.add(USER_DTO_1.getId(), USER_1_PROJECT_DTO_1);
        service.updateByIndex(USER_DTO_1.getId(), 1, PROJECT_NAME, PROJECT_DESCRIPTION);
        Assert.assertEquals(PROJECT_NAME, service.findProjectById(USER_DTO_1.getId(), USER_1_PROJECT_DTO_1.getId()).getName());
        Assert.assertEquals(PROJECT_DESCRIPTION, service.findProjectById(USER_DTO_1.getId(), USER_1_PROJECT_DTO_1.getId()).getDescription());
    }

    @Test
    public void changeStatusById() {
        service.add(USER_DTO_1.getId(), USER_1_PROJECT_DTO_1);
        service.changeStatusById(USER_DTO_1.getId(), USER_1_PROJECT_DTO_1.getId(), Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, service.findProjectById(USER_DTO_1.getId(), USER_1_PROJECT_DTO_1.getId()).getStatus());
    }

    @Test
    public void changeStatusByIndex() {
        service.add(USER_DTO_1.getId(), USER_1_PROJECT_DTO_1);
        service.changeStatusByIndex(USER_DTO_1.getId(), 1, Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, service.findProjectById(USER_DTO_1.getId(), USER_1_PROJECT_DTO_1.getId()).getStatus());
    }

    @After
    public void clearData() {
        service.clear();
    }

}
