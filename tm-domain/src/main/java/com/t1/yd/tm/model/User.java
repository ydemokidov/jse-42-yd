package com.t1.yd.tm.model;

import com.t1.yd.tm.enumerated.Role;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "users")
@NoArgsConstructor
public final class User extends AbstractEntity {

    @NotNull
    @OneToMany(mappedBy = "user")
    private List<Project> projects = new ArrayList<>();

    @NotNull
    @OneToMany(mappedBy = "user")
    private List<Task> tasks = new ArrayList<>();

    @NotNull
    @OneToMany(mappedBy = "user")
    private List<Session> sessions = new ArrayList<>();

    @NotNull
    @Column(length = 150, nullable = false)
    private String login;

    @NotNull
    @Column(name = "pwd_hash", length = 500, nullable = false)
    private String passwordHash;

    @NotNull
    @Column(length = 100, nullable = false)
    private String email;

    @Nullable
    @Column(name = "fst_name", length = 100)
    private String firstName;

    @Nullable
    @Column(name = "last_name", length = 100)
    private String lastName;

    @Nullable
    @Column(name = "mid_name", length = 100)
    private String middleName;

    @NotNull
    @Column(length = 50, nullable = false)
    @Enumerated(EnumType.STRING)
    private Role role = Role.USUAL;

    @NotNull
    @Column(nullable = false)
    private Boolean locked = false;

}