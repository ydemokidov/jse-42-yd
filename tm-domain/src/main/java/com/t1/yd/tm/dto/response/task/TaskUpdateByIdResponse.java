package com.t1.yd.tm.dto.response.task;

import com.t1.yd.tm.dto.model.TaskDTO;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public class TaskUpdateByIdResponse extends AbstractTaskResponse {

    public TaskUpdateByIdResponse(@NotNull final TaskDTO taskDTO) {
        super(taskDTO);
    }

    public TaskUpdateByIdResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

}
