package com.t1.yd.tm.exception.system;

import com.t1.yd.tm.exception.AbstractException;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
public class AbstractSystemException extends AbstractException {

    public AbstractSystemException(@NotNull String message) {
        super(message);
    }

    public AbstractSystemException(@NotNull String message, @NotNull Throwable cause) {
        super(message, cause);
    }

    public AbstractSystemException(@NotNull Throwable cause) {
        super(cause);
    }

    public AbstractSystemException(@NotNull String message, @NotNull Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
